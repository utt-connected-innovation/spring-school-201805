from flask import Flask, jsonify, render_template, request
from flasgger import Swagger
from jinja2 import TemplateNotFound
import os
import yaml

from scicap.recipes.bigform.blueprint import bigform
from scicap.recipes.bigform import blueprint
from scicap import utils

app = Flask(__name__)
swagger = Swagger(app)

#####################################

# Write here your API

@app.route('/api/save/<collection>', methods=['POST'])
def api_save_schema(collection):
    """
    ...
    """
    doc = blueprint.schema.save(collection, request.json)

    # Write
    certifpath = '/datas/certif/%s' %(collection)
    if not os.path.exists(certifpath):
        os.makedirs(certifpath)
    filepath = os.path.join(certifpath, "%s.yaml" %(doc['_id']))
    with open(filepath, 'w') as yaml_file:
        yaml.dump(doc, yaml_file, default_flow_style=False)
    return jsonify(dict(doc_id=doc['_id']))


@app.route('/api/edit/<collection>/<doc_id>', methods=['POST'])
def api_edit_schema(collection, doc_id):
    """
    Example ...
    ---
    definitions:
      Object:
        type: object
    responses:
      200:
        description: A simple object
        schema:
          $ref: '#/definitions/Object'
    """
    doc = blueprint.schema.save(collection, request.json, doc_id)

    # Write
    certifpath = '/datas/certif/%s' %(collection)
    if not os.path.exists(certifpath):
        os.makedirs(certifpath)
    filepath = os.path.join(certifpath, "%s.yaml" %(doc['_id']))
    with open(filepath, 'w') as yaml_file:
        yaml.dump(doc, yaml_file, default_flow_style=False)
    return jsonify(dict(doc_id=doc['_id']))


#####################################

# Fix apidocs access
app.wsgi_app = utils.ReverseProxied(app.wsgi_app)

app.register_blueprint(bigform)
app.run(host="0.0.0.0", port=5000, debug=True)
