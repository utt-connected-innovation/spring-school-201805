from flask import Flask, jsonify, render_template
from flasgger import Swagger
from jinja2 import TemplateNotFound

from scicap.recipes.pdf_services.blueprint import pdf_services
from scicap import utils

app = Flask(__name__)
swagger = Swagger(app)

#####################################

# Write here your API

@app.route('/info', methods=['GET'])
def info():
    """
    Example endpoint
    This is the specifications.
    ---
    definitions:
      SimpleDict:
        type: object
        properties:
          a:
            type: integer
            example: 33
          b:
            type: integer
            example: 344
    responses:
      200:
        description: A simple dict
        schema:
          $ref: '#/definitions/SimpleDict'
        examples:
          a: 33
          b: 344
    """
    return jsonify(dict(a=33, b=355))

#####################################

# Fix apidocs access
app.wsgi_app = utils.ReverseProxied(app.wsgi_app)

app.register_blueprint(pdf_services)
app.run(host="0.0.0.0", port=5000, debug=True)
